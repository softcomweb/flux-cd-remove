resource "tls_private_key" "flux" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}

data "gitlab_project" "this" {
  path_with_namespace = "${var.gitlab_group}/${var.gitlab_project}"
}

resource "gitlab_deploy_key" "this" {
  project  = data.gitlab_project.this.id
  title    = "Flux v2 Cd deploy key for orbstack kind cluster"
  key      = tls_private_key.flux.public_key_openssh
  can_push = true
}

resource "flux_bootstrap_git" "this" {
  depends_on = [gitlab_deploy_key.this]
  path       = "infra/prod"
  version = "v2.1.2"
  components_extra = ["image-reflector-controller","image-automation-controller"]
}

#module "s3" {
#  source = "../modules/s3-bucket"
#  environment = var.environment
#}

#module "dynamodb" {
#  source = "../modules/dynamodb"
#  table_name = "terraform-${var.environment}-state-lock"
#}
