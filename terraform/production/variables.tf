variable "gitlab_token" {
  sensitive = true
  type      = string
}

variable "gitlab_group" {
  type = string
}

variable "gitlab_project" {
  type = string
}

variable "kube_context" {
  type = string
}

variable "environment" {
  type = string
  description = "Infra environmment"
  default = "infra"
}
